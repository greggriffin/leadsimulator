function varargout = leadSimulator(varargin)
% LEADSIMULATOR MATLAB code for leadSimulator.fig
%      LEADSIMULATOR, by itself, creates a new LEADSIMULATOR or raises the existing
%      singleton*.
%
%      H = LEADSIMULATOR returns the handle to a new LEADSIMULATOR or the handle to
%      the existing singleton*.
%
%      LEADSIMULATOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LEADSIMULATOR.M with the given input arguments.
%
%      LEADSIMULATOR('Property','Value',...) creates a new LEADSIMULATOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before leadSimulator_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to leadSimulator_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help leadSimulator

% Last Modified by GUIDE v2.5 13-Feb-2018 14:11:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @leadSimulator_OpeningFcn, ...
                   'gui_OutputFcn',  @leadSimulator_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before leadSimulator is made visible.
function leadSimulator_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to leadSimulator (see VARARGIN)
handles.bounded = zeros(1,7);
handles.bounded(5:7) = 1;
handles.R = 0;
handles.RValue.String = num2str(handles.R);
handles.ResistanceSlider.Value = handles.R;
handles.L = 250e-9;
handles.LValue.String = num2str(handles.L*1e9);
handles.InductanceSlider.Value = handles.L*1e9;
handles.G = 0;
handles.GValue.String = num2str(handles.G*1e3);
handles.AdmittanceSlider.Value = handles.G*1e3;
handles.C = 100e-12;
handles.CValue.String = num2str(handles.C*1e12);
handles.CapacitanceSlider.Value = handles.C*1e12;
handles.Re = 50;
handles.ReValue.String = num2str(handles.Re);
handles.Ce = 100000e-12;
handles.CeValue.String = num2str(handles.Ce*1e12);
handles.Lfix =42e-9;
handles.LFixValue.String = num2str(handles.Lfix*1e9);
handles.l = 0.6;
handles.lengthValue.String = num2str(handles.l*1e2);
handles.Fmin = 50e6;
handles.FStart.String = num2str(handles.Fmin/1e6);
handles.Fmax = 100e6;
handles.FStop.String = num2str(handles.Fmax/1e6);
handles.freq = linspace(handles.Fmin,handles.Fmax,200);
smith = TL2Smith(handles.R,handles.L,handles.G,handles.C,handles.Re,handles.Ce,handles.Lfix,handles.l,handles.freq);
handles.smithPlot = smithchart(smith);
hold on

addlistener(handles.InductanceSlider, 'Value', 'PostSet',@InductanceSlider_Changing_Callback);
addlistener(handles.ResistanceSlider, 'Value', 'PostSet',@ResistanceSlider_Changing_Callback);
addlistener(handles.AdmittanceSlider, 'Value', 'PostSet',@AdmittanceSlider_Changing_Callback);
addlistener(handles.CapacitanceSlider, 'Value', 'PostSet',@CapacitanceSlider_Changing_Callback);

update(handles);
% Choose default command line output for leadSimulator
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);



% --- Outputs from this function are returned to the command line.
function varargout = leadSimulator_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%% ******************inductance block*************************
function InductanceSlider_Callback(hObject, eventdata, handles)
    handles.L = get(hObject,'Value')/1e9;
    update(handles);
    guidata(hObject,handles);
function InductanceSlider_Changing_Callback(hObject, eventdata)
    handles = guidata(eventdata.AffectedObject);
    handles.L = get(eventdata.AffectedObject, 'Value')/1e9;
    update(handles);
function InductanceSlider_CreateFcn(hObject, eventdata, handles)
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
function LValue_Callback(hObject, eventdata, handles)
    var = str2double(get(hObject,'String'));
    if (handles.InductanceSlider.Min <= var) && (handles.InductanceSlider.Max >= var)
        handles.L = str2double(get(hObject,'String'))/1e9;
    elseif var > handles.InductanceSlider.Max
        handles.InductanceSlider.Max = var;
        handles.L = handles.InductanceSlider.Max/1e9;
    elseif var < handles.InductanceSlider.Max
        handles.InductanceSlider.Min = var;
        handles.L = handles.InductanceSlider.Min/1e9;
    end
    update(handles);
    guidata(hObject,handles);
function LValue_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
function LMax_Callback(hObject, eventdata, handles)
    var = str2double(get(hObject, 'String'));
    if var >= handles.L * 1e9
        handles.InductanceSlider.Max = var;
    else
        handles.InductanceSlider.Max = handles.L*1e9;
    end
    update(handles);
    guidata(hObject,handles);
function LMax_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function LMin_Callback(hObject, eventdata, handles)
    var = str2double(get(hObject, 'String'));
    if var <= handles.L * 1e9
        handles.InductanceSlider.Min = var;
    else
        handles.InductanceSlider.Min = handles.L*1e9;
    end
    guidata(hObject,handles);
    update(handles);
function LMin_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%% ******************admittance block*************************
function AdmittanceSlider_Callback(hObject, eventdata, handles)
    handles.G = get(hObject,'Value')/1e3;
    update(handles);
    guidata(hObject, handles);

function AdmittanceSlider_Changing_Callback(hObject, eventdata)
    handles = guidata(eventdata.AffectedObject);
    handles.G = get(eventdata.AffectedObject, 'Value')/1e3;
    update(handles);

function AdmittanceSlider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function GValue_Callback(hObject, eventdata, handles)
var = str2double(get(hObject,'String'));
if (handles.AdmittanceSlider.Min <= var) && (handles.AdmittanceSlider.Max >= var)
    handles.G = str2double(get(hObject,'String'))/1e3;
elseif var > handles.AdmittanceSlider.Max
    handles.AdmittanceSlider.Max = var;
    handles.G = handles.AdmittanceSlider.Max/1e3;
elseif var < handles.AdmittanceSlider.Max
    handles.AdmittanceSlider.Min = var;
    handles.G = handles.AdmittanceSlider.Min/1e3;
end
update(handles);
guidata(hObject, handles);

function GValue_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function GMin_Callback(hObject, eventdata, handles)
var = str2double(get(hObject, 'String'));
if var <= handles.G*1e3
    handles.AdmittanceSlider.Min = var;
else
    handles.AdmittanceSlider.Min = handles.G*1e3;
end
update(handles);
guidata(hObject,handles);

function GMin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to GMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function GMax_Callback(hObject, eventdata, handles)
var = str2double(get(hObject, 'String'));
if var >= handles.G*1e3
    handles.AdmittanceSlider.Max = var;
else
    handles.AdmittanceSlider.Max = handles.G*1e3;
end
update(handles);
guidata(hObject,handles);

function GMax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to GMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end









%% ******************capacitance block*************************
function CapacitanceSlider_Callback(hObject, eventdata, handles)
    handles.C = get(hObject,'Value')/1e12;
    update(handles);
    guidata(hObject, handles);

function CapacitanceSlider_Changing_Callback(hObject, eventdata)
    handles = guidata(eventdata.AffectedObject);
    handles.C = get(eventdata.AffectedObject, 'Value')/1e12;
    update(handles);
    
function CapacitanceSlider_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function CValue_Callback(hObject, eventdata, handles)
    var = str2double(get(hObject,'String'));
    if (handles.CapacitanceSlider.Min <= var) && (handles.CapacitanceSlider.Max >= var)
        handles.C = str2double(get(hObject,'String'))/1e12;
    elseif var > handles.CapacitanceSlider.Max
        handles.CapacitanceSlider.Max = var;
        handles.C = handles.CapacitanceSlider.Max/1e12;
    elseif var < handles.CapacitanceSlider.Max
        handles.CapacitanceSlider.Min = var;
        handles.C = handles.CapacitanceSlider.Min/1e12;
    end
    update(handles);
    guidata(hObject, handles);

function CValue_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function CMin_Callback(hObject, eventdata, handles)
    var = str2double(get(hObject, 'String'));
    if var <= handles.C*1e12
        handles.CapacitanceSlider.Min = var;
    else
        handles.CapacitanceSlider.Min = handles.C*1e12;
    end
    update(handles);
    guidata(hObject,handles);

function CMin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function CMax_Callback(hObject, eventdata, handles)
    var = str2double(get(hObject, 'String'));
    if var >= handles.C*1e12
        handles.CapacitanceSlider.Max = var;
    else
        handles.CapacitanceSlider.Max = handles.C*1e12;
    end
    update(handles);
    guidata(hObject,handles);

function CMax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




%% ******************resistance block*************************
function ResistanceSlider_Callback(hObject, eventdata, handles)
    handles.R = get(hObject,'Value');
    update(handles);
    guidata(hObject, handles);

function ResistanceSlider_Changing_Callback(hObject, eventdata)
    handles = guidata(eventdata.AffectedObject);
    handles.R = get(eventdata.AffectedObject, 'Value');
    update(handles);

function ResistanceSlider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function RValue_Callback(hObject, eventdata, handles)
    handles.R = str2double(get(hObject,'String'));
    update(handles);
    guidata(hObject, handles);

function RValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function RMin_Callback(hObject, eventdata, handles)
    var = str2double(get(hObject, 'String'));
    if var <= handles.R
        handles.ResistanceSlider.Min = var;
    else
        handles.ResistanceSlider.Min = var;
        handles.R = var;
    end
    update(handles);
    guidata(hObject,handles);

function RMin_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function RMax_Callback(hObject, eventdata, handles)
    % hObject    handle to RMax (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'String') returns contents of RMax as text
    %        str2double(get(hObject,'String')) returns contents of RMax as a double
    var = str2double(get(hObject, 'String'));
    if var >= handles.R
        handles.ResistanceSlider.Max = var;
    else
        handles.ResistanceSlider.Max = handles.R;
    end
    update(handles);
    guidata(hObject,handles);

function RMax_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% ******************frequency block*************************
function FStart_Callback(hObject, eventdata, handles)
% hObject    handle to FStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of FStart as text
%        str2double(get(hObject,'String')) returns contents of FStart as a double
var = str2double(get(hObject,'String'));
if var*1e6 < handles.Fmax
    handles.Fmin = var*1e6;
    handles.FStart.String = num2str(var);

    if isfield(handles,'zMeas')
        handles.zMeas = interp1(handles.freqFull,handles.zMeasFull,linspace(handles.Fmin,handles.Fmax,length(handles.zMeas)));
        set(handles.measPlot,'XData',real(z2gamma(handles.zMeas,50)),'YData',imag(z2gamma(handles.zMeas,50)));
    end
    handles.freq = linspace(handles.Fmin,handles.Fmax,201);
    smith = TL2Smith(handles.R,handles.L,handles.G,handles.C,handles.Re,handles.Ce,handles.Lfix,handles.l,handles.freq);
    set(handles.smithPlot,'XData',real(smith),'YData',imag(smith)); 
    guidata(hObject, handles);
else
    handles.FStart.String = num2str(handles.Fmin/1e6);
    guidata(hObject, handles);
end
function FStart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function FStop_Callback(hObject, eventdata, handles)

var = str2double(get(hObject,'String'));
if var*1e6 > handles.Fmin
    handles.Fmax = var*1e6;
    handles.FStop.String = num2str(var);
    if isfield(handles,'zMeas')
        handles.zMeas = interp1(handles.freqFull,handles.zMeasFull,linspace(handles.Fmin,handles.Fmax,length(handles.zMeas)));
        set(handles.measPlot,'XData',real(z2gamma(handles.zMeas,50)),'YData',imag(z2gamma(handles.zMeas,50)));
    end
    handles.freq = linspace(handles.Fmin,handles.Fmax,201);
    smith = TL2Smith(handles.R,handles.L,handles.G,handles.C,handles.Re,handles.Ce,handles.Lfix,handles.l,handles.freq);
    set(handles.smithPlot,'XData',real(smith),'YData',imag(smith)); 
    guidata(hObject, handles);
else
    handles.FStop.String = num2str(handles.Fmax/1e6);
    guidata(hObject, handles);
end
function FStop_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ReValue_Callback(hObject, eventdata, handles)
    handles.Re = str2double(get(hObject,'String'));
    update(handles);
    guidata(hObject,handles);
function ReValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ReValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function CeValue_Callback(hObject, eventdata, handles)
    handles.Ce = str2double(get(hObject,'String'))*1e-12;
    guidata(hObject,handles);
    update(handles);    
function CeValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CeValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function LFixValue_Callback(hObject, eventdata, handles)
    handles.Lfix = str2double(get(hObject,'String'))*1e-9;
    guidata(hObject,handles);
    update(handles);    
function LFixValue_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function lengthValue_Callback(hObject, eventdata, handles)
    handles.l = str2double(get(hObject,'String'))*1e-2;
    update(handles);
    guidata(hObject,handles);
function lengthValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lengthValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


    

%% ******************import block*************************
function readDataButton_Callback(hObject, eventdata, handles)

[filename1,filepath1]=uigetfile({'*.*','All Files'},'Select Data File 1');
cd(filepath1);
load(filename1);

handles.fValue.String='';
handles.Fmin = min(freq);
handles.FStart.String = num2str(handles.Fmin/1e6);
handles.Fmax = max(freq);
handles.FStop.String = num2str(handles.Fmax/1e6);
handles.freq = linspace(handles.Fmin,handles.Fmax,201);
smith = TL2Smith(handles.R,handles.L,handles.G,handles.C,handles.Re,handles.Ce,handles.Lfix,handles.l,handles.freq);
set(handles.smithPlot,'XData',real(smith),'YData',imag(smith)); 
if isfield(handles, 'measPlot')
    set(handles.measPlot,'XData',real(z2gamma(zMeas,50)),'YData',imag(z2gamma(zMeas,50)));
else
    handles.measPlot = smithchart(z2gamma(zMeas,50));
end
handles.zMeas = zMeas;
handles.zMeasFull = zMeas;
handles.freqFull = handles.freq;
guidata(hObject, handles);



% --- Executes on button press in runFitButton.
function runFitButton_Callback(hObject, eventdata, handles)
% hObject    handle to runFitButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isfield(handles,'zMeas')
    set(hObject,'String','Running...');
    pause(1)
    refresh(gcf);

    guess(1) = handles.R;
    guess(2) = handles.L;
    guess(3) = handles.G;
    guess(4) = handles.C;
    guess(5) = handles.Re;
    guess(6) = handles.Ce;
    guess(7) = handles.Lfix;
    
    lb = [handles.ResistanceSlider.Min,...
        handles.InductanceSlider.Min/1e9,...
        handles.AdmittanceSlider.Min/1e3,...
        handles.CapacitanceSlider.Min/1e12,...
        0,...
        0,...
        0];
    
    ub = [handles.ResistanceSlider.Max,...
        handles.InductanceSlider.Max/1e9,...
        handles.AdmittanceSlider.Max/1e3,...
        handles.CapacitanceSlider.Max/1e12,...
        inf,...
        inf,...
        inf];
        

    [params,f] = searchTLParams(handles.bounded,lb,ub,handles.zMeas,handles.freq,handles.l,guess);
    
    
    if isempty(handles.fValue.String)
        handles.ParamsOld = params;
        handles.fValue.String = num2str(1/f);

        handles.R = params(1);
        handles.L = params(2);
        handles.G = params(3);
        handles.C = params(4);
        handles.Re = params(5);
        handles.Ce = params(6);
        handles.Lfix = params(7);
        guidata(hObject,handles);        
    else
        if (f < 1/str2double(handles.fValue.String))
            handles.fValue.String = num2str(1/f);

            handles.R = params(1);
            handles.L = params(2);
            handles.G = params(3);
            handles.C = params(4);
            handles.Re = params(5);
            handles.Ce = params(6);
            handles.Lfix = params(7);
            handles.ParamsOld = params;
            guidata(hObject,handles);
        else
            paramsOld = handles.ParamsOld;
            handles.R = paramsOld(1);
            handles.L = paramsOld(2);
            handles.G = paramsOld(3);
            handles.C = paramsOld(4);
            handles.Re = paramsOld(5);
            handles.Ce = paramsOld(6);
            handles.Lfix = paramsOld(7);
            guidata(hObject,handles);
        end
    end
    set(hObject,'String','Run Fit');

else
    msgbox('You must import test data before fitting can be performed')
end
    pause(0.1)
    update(handles);
    


% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, eventdata, handles)
if isfield(handles,'zMeas')
    fn = uiputfile('*.mat','Save analyzed results as:');
    save([fn(1:strfind(fn,'.mat')-1),'_analyzed.mat'],'-struct','handles','R','L','G','C','Re','Ce','Lfix','l','freq','freqFull','zMeas','zMeasFull');
else
    msgbox('You must import test data before you can save analyzed results.')
end

% --- Executes on button press in ReFixed.
function ReFixed_Callback(hObject, eventdata, handles)
% hObject    handle to ReFixed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ReFixed
handles.bounded(5) = get(hObject,'Value');
guidata(hObject,handles);


% --- Executes on button press in CeFixed.
function CeFixed_Callback(hObject, eventdata, handles)
% hObject    handle to CeFixed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CeFixed
handles.bounded(6) = get(hObject,'Value');
guidata(hObject,handles);

% --- Executes on button press in LfixFixed.
function LfixFixed_Callback(hObject, eventdata, handles)
% hObject    handle to LfixFixed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of LfixFixed
handles.bounded(7) = get(hObject,'Value');
guidata(hObject,handles);


% --- Executes on button press in leadPerformance.
function leadPerformance_Callback(hObject, eventdata, handles)
% hObject    handle to leadPerformance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.Fmin <= 64e6 && handles.Fmax >= 64e6
    f0 = 64e6;
else
    f0 = 128e6;
end

[worstHeat,uniHeat] = getLeadHeating(handles,f0,true);
