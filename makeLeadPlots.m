function makeLeadPlots(lead,c,scale)

f0 = 64e6;

leadList = {
    lead};


clear *Heat
%figure

for m = 1:length(leadList)
    for i = 1:8
        fn = [leadList{m},'_EL',num2str(i),'_analyzed.mat'];
        [worstHeat(:,i),uniHeat(:,i),lengths] = getLeadHeating(fn,f0,false);
%         plot(lengths,uniHeat(:,i))
%         hold on
    end

    
    tier1Max = max(uniHeat');%mean(uniHeat,2) + 2*std(uniHeat,0,2);
    tier1Min = min(uniHeat');%mean(uniHeat,2) - 2*std(uniHeat,0,2);
    fill([lengths*100,fliplr(lengths*100)],[tier1Min/scale,fliplr(tier1Max)/scale],c)
    alpha 0.3
    %axis([min(lengths*100) max(lengths*100) 0 1])
    hold on
end




xlabel('Length (cm)')
ylabel('Relative Tier 1 Heating (a.u.)')
%axis([min(lengths)*100 max(lengths)*100 0 1])
