function C = smithDiff(params,l,freq,s11meas)





R = params(1);
L = params(2);
G = params(3);
C = params(4);
Re = params(5);
Ce = params(6);
Lfix = params(7);
%Rs = params(8);

smith = TL2Smith(R,L,G,C,Re,Ce,Lfix,l,freq);

C = sum(abs(s11meas - smith));


% subplot(1,3,1)
% plot(freq,abs(s11meas))
% hold on
% plot(freq,abs(smith))
% hold off
% title('magnitude')
% axis([min(freq) max(freq) 0 1])
% drawnow
% 
% 
% subplot(1,3,2)
% plot(freq,angle(s11meas))
% hold on
% plot(freq,angle(smith))
% hold off
% title('phase')
% axis([min(freq) max(freq) -pi pi])
% 
% subplot(1,3,3)
% smithchart(s11meas)
% %plot(real(s11meas),imag(s11meas))
% hold on
% smithchart(smith)
% %plot(real(smith),imag(smith))
% %axis([-1 1 -1 1])
% hold off
% title('Smith')
% 
% drawnow


