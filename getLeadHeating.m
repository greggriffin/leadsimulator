function [worstHeat,uniHeat,lengths] = getLeadHeating(fn,f0,plots)

if isstruct(fn)
    R = fn.R;
    L = fn.L;
    G = fn.G;
    C = fn.C;
    Re = fn.Re;
    Ce = fn.Ce;
    l = fn.l;
else
    load(fn,'R','L','G','C','Re','Ce','l');
end

w0 = 2*pi*f0;

Z = R + 1i*w0*L;
Y = G + 1i*w0*C;

Xe = 1./(1i*w0*Ce);
Ze = Re + Xe;

Zc = 0.4 - 1i*4;





%% plotting heating behavior
lengths = linspace(0.3,1);
for i = 1:length(lengths)
    [~,TF] = TL2TF(Z,Y,Ze,Zc,lengths(i));
    worstHeat(i) = sum(abs(TF));
    uniHeat(i) = abs(sum(TF));
end


if plots
%% plotting TF


%     [x,TF] = TL2TF(Z,Y,Ze,Zc,l);
%     figure
%     plot(x*100,abs(TF))
%     xlabel('Position (cm)')
%     ylabel('|TF|')

    figure
%     plot(lengths*100,worstHeat)
%     hold on
    plot(lengths*100,uniHeat)
    xlabel('Length (cm)')
    ylabel('Tier 1 Heating (a.u.)')
    axis([30 110 0 max(uniHeat)*1.1])
    %legend('Worst case possible','Tier 1')
end
