function [worstHeat,uniHeat] = getLeadHeating(fn,f0)

load(fn,'R','L','G','C','Re','Ce','l');

w0 = 2*pi*f0;


Z = R + 1i*w0*L;
Y = G + 1i*w0*C;

Xe = 1./(1i*w0*Ce);
Ze = Re + Xe;

Zc = 0.4 - 1i*4;

%[x,TF] = TL2TF(Z,Y,Ze,Zc,l);

lengths = linspace(0.3,1);
for i = 1:length(lengths)
    [~,TF] = TL2TF(Z,Y,Ze,Zc,lengths(i));
    worstHeat(i) = sum(abs(TF));
    unitHeat(i) = abs(sum(TF));
end

