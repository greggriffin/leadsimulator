function smith = TL2Smith(R,L,G,C,Re,Ce,Lfix,l,freq)
%
%This function generates a smith chart of the reflection coefficient that
%would be measured for a given MRI lead with defined parameters
%
%   INPUTS
%   R - characteristic resistance of the cable [Ohm/m]
%   L - characteristic inductance of the cable [H/m]
%   G - characteristic shunt conductance of the cable [S/m]
%   C - characteristic capacitance of the cable [F/m]
%   Re - resistance of the load [Ohms]
%   Ce - capacitance of the load [Farad]
%   Rs - resistance of the saline [Ohms/m]
%   Lfix - inductance of the fixture [Henry]
%   N - the number of points over which to segment (mesh) the lead
%   l - the length of the lead
%   freq - the frequency range over which to calculate
%
%   OUTPUTS
%   smith - complex vector containing reflection coefficient of the lead
%
%   v0.1: Greg Griffin - Oct 2 2017
%   v0.2: Greg Griffin - Dec 5 2017 (bug fixes)

%close all


w = 2*pi*freq;


%% transmission line approach
Zp = R + 1i*w*L;
Yp = G + 1i*w*C;

Zc = sqrt(Zp./Yp);
gamma = sqrt(Zp.*Yp);

Xe = (1./(1i*w*Ce)).';
Zl = (Re + Xe)';


Zin = Zc.*(Zl + Zc.*tanh(gamma*-l))./(Zc + Zl.*tanh(gamma*-l));
Zfix = (1i*w*Lfix);
Zin = Zin + Zfix;

%% final transformation
Z0 = 50;


if sum(isnan(Zin)) == 0
    smith = z2gamma(Zin,Z0)';
else
    pause
end




