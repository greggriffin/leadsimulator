function plotLeadTF(x,TF)



subplot(2,1,1)
plot(x*100,abs(TF))
xlabel('Position (cm)')
ylabel('TF Magnitude (a.u.)')


subplot(2,1,2)
plot(x*100,angle(TF))
xlabel('Position (cm)')
ylabel('\angle TF')



