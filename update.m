function update(handles)

%%%%%%%%%%%%%%resistance
if handles.R > handles.ResistanceSlider.Max
    handles.ResistanceSlider.Max = handles.R;
    
elseif handles.R < handles.ResistanceSlider.Min
    handles.ResistanceSlider.Min = handles.R;
    
end
handles.RMax.String = num2str(ceil(handles.ResistanceSlider.Max));
handles.RMin.String = num2str(floor(handles.ResistanceSlider.Min));
handles.RValue.String = num2str(handles.R);
handles.ResistanceSlider.Value = handles.R;

lScale = 1e9;
%%%%%%%%%%%%%%%%%%%%inductance
if handles.L*lScale > handles.InductanceSlider.Max
    handles.InductanceSlider.Max = handles.L*lScale;
elseif handles.L*lScale < handles.InductanceSlider.Min
    handles.InductanceSlider.Min = handles.L*lScale;
end
handles.LMax.String = num2str(round(handles.InductanceSlider.Max));
handles.LMin.String = num2str(floor(handles.InductanceSlider.Min));
handles.LValue.String = num2str(round(handles.L*lScale));
handles.InductanceSlider.Value = handles.L*lScale;




gScale = 1e3;
%%%%%%%%%%%%%%%%%admittance
if handles.G*gScale > handles.AdmittanceSlider.Max
    handles.AdmittanceSlider.Max = handles.G*gScale;
elseif handles.G*gScale < handles.AdmittanceSlider.Min
    handles.AdmittanceSlider.Min = handles.G*gScale;
end
handles.GMin.String = num2str(floor(handles.AdmittanceSlider.Min));
handles.GMax.String = num2str(ceil(handles.AdmittanceSlider.Max));
handles.GValue.String = num2str(handles.G*gScale);
handles.AdmittanceSlider.Value = handles.G*gScale;



cScale = 1e12;
%%%%%%%%%%%%%%%capacitance
if handles.C*cScale > handles.CapacitanceSlider.Max
    handles.CapacitanceSlider.Max = handles.C*cScale;
elseif handles.C*cScale < handles.CapacitanceSlider.Min
    handles.CapacitanceSlider.Min = handles.C*cScale;
end
handles.CMax.String = num2str(ceil(handles.CapacitanceSlider.Max));
handles.CMin.String = num2str(floor(handles.CapacitanceSlider.Min));
handles.CValue.String = num2str(round(handles.C*cScale));
handles.CapacitanceSlider.Value = handles.C*cScale;


handles.ReValue.String = num2str(handles.Re);
handles.CeValue.String = num2str(handles.Ce*cScale);
handles.LFixValue.String = num2str(handles.Lfix*lScale);


lengthScale = 1e2;
%%%%%%%%%%length
handles.lengthValue.String = num2str(handles.l*lengthScale);

smith = TL2Smith(handles.R,handles.L,handles.G,handles.C,handles.Re,handles.Ce,handles.Lfix,handles.l,handles.freq);
set(handles.smithPlot,'XData',real(smith),'YData',imag(smith)); 
