function [x,TF] = TL2TF(Z,Y,Ze,Zc,l)
%takes in transmission line parameters, load impedances, and length, and
%calculates transfer function

z0 = sqrt(Z./Y);
k = sqrt(-Z.*Y);



rhoc = (Zc - z0)./(Zc + z0);
rhoe = (Ze - z0)./(Ze + z0);

x = linspace(0,l); %x position on the lead, x=0 corresponds to the can, x=l corresponds to the electrode

N = 20;
TF = zeros(size(x));

for n = 1:N
    TF = TF + (-1)^(n-1)*rhoc^floor((n-1)/2)*rhoe^(floor(n/2))*exp(-1i*k*(floor(n/2)*2*l + (-1)^(n-1)*x));
end
