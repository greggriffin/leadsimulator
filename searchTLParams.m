function [params,fval] = searchTLParams(bounded,lb,ub,zmeas,freq,l,guess)
%
%   lb - the lower bounds for the fit
%   ub - the upper bounds for the fit
%   bounded -   logical vector containing a 0 if the parameter should be
%               allowed to vary, and a 1 if it should be fixed

%% interpolating measurements to make 201 points
zmeas = interp1(freq,zmeas,linspace(min(freq),max(freq),201));
freq = linspace(min(freq),max(freq),201);


%% converting to reflection coefficient
Z0 = 50;
s11 = z2gamma(zmeas,Z0).';


%% fitting options
options = optimset('display','final','TolFun',eps/1e10,'TolX',eps/1e10,'MaxFunEvals',1e6,'MaxIter',1e4);


%% fitting calls, with various bounds on parameters depending on test configuration

for i = find(bounded)
    lb(i) = guess(i);
    ub(i) = lb(i);
end

guess = guess + randn(size(guess)).*guess*0.1;

[params,fval] = fminsearchbnd(@(params) smithDiff(params,l,freq,s11),...
    guess, lb, ub, options);

